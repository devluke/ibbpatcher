# ibbpatcher

Tool to automatically patch iBEC/iBSS for 64-bit iOS devices

## Usage

`./ibbpatcher`

You will be prompted to enter a device identifier (ex. ipad4,4) and desired iOS version (ex. 10.3.3).

That's *it*.

## Progress

This tool is not yet complete. Currently it:

- [x] Downloads and extracts IPSW
- [x] Extracts iBEC/iBSS from IPSW
- [ ] Gets firmware keys for device/version
- [ ] Decrypts iBEC/iBSS using firmware keys
- [ ] Extracts raw binary from decrypted images
- [ ] Patches iBEC/iBSS using iBoot64Patcher
- [ ] ... (still learning the process)
