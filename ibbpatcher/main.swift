//
//  main.swift
//  ibbpatcher
//
//  Created by Luke Chambers on 10/9/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import Rainbow
import FileKit
import ShellOut

// MARK: - Utilities

extension String {
    var pretty: String { return "=> \(self)".cyan.bold }
    var warn: String { return "=> \(self)".yellow.bold }
    var error: String { return "** \(self)".red.bold }
}

extension Path {
    var path: String { return "\(self)" }
}

func readLine(string: String) -> String {
    while true {
        print(string, terminator: "")
        if let input = readLine() {
            let trimmed = input.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmed.count != 0 { return trimmed }
        }
    }
}

func verifyFile(at path: Path, print printResult: Bool = false) -> Bool {
    let exists = path.exists
    if printResult {
        print(exists ? "yes" : "no")
    }
    return exists
}

func verifyFiles(at paths: [Path], print printResult: Bool) -> Bool {
    var results = [Bool]()
    paths.forEach { path in
        results.append(verifyFile(at: path))
    }
    let result = !results.contains(false)
    if printResult {
        print(result ? "yes" : "no")
    }
    return result
}

// MARK: - Get device info from user

let identifier = readLine(string: "Device identifier: ".pretty).lowercased()
let version = readLine(string: "Desired iOS version: ".pretty)

// MARK: - Create and switch to work directory

print("Creating work directory...".pretty)

let workDir = Path("ibbpatcher-work")
do {
    try workDir.createDirectory()
} catch {
    print("Failed to create work directory".error)
    exit(1)
}

Path.current = workDir

// MARK: - Download and verify IPSW

/*print("Downloading IPSW...".pretty)
print("This may take a while, Apple's download servers are very slow".warn)

let ipswURL = "http://api.ipsw.me/v2.1/\(identifier)/\(version)/url/dl"
*/let ipswPath = Path("ipsw.ipsw")/*
do {
    try shellOut(to: "wget", arguments: [ipswURL, "-O", ipswPath.path])
} catch {
    print("Failed to download IPSW".error)
    exit(1)
}*/

// DEBUG
print("Put ipsw.ipsw in work directory: ".pretty, terminator: "")
let _ = readLine()
// END DEBUG

print("Verifying IPSW... ".pretty, terminator: "")

if !verifyFile(at: Path(ipswPath.path), print: true) {
    print("Failed to verify IPSW".error)
    exit(1)
}

// MARK: - Extract IPSW and verify directory

print("Extracting IPSW...".pretty)
do {
    try shellOut(to: "unzip", arguments: [ipswPath.path, "-d", "ipsw"])
} catch {
    print("Failed to extract IPSW".error)
    exit(1)
}

print("Verifying IPSW directory... ".pretty, terminator: "")

let ipswDFUPath = Path("ipsw") + "Firmware" + "dfu"
if !verifyFile(at: ipswDFUPath, print: true) {
    print("Failed to verify IPSW directory".error)
    exit(1)
}

// MARK: - Extract iBEC/iBSS from IPSW directory

print("Extracting iBEC/iBSS...".pretty)

let firstIdentifier = identifier.split(separator: ",")[0]

let iBECPath = Path("iBEC.\(firstIdentifier).RELEASE.im4p")
let iBSSPath = Path("iBSS.\(firstIdentifier).RELEASE.im4p")

do {
    try (ipswDFUPath + iBECPath) +>> (Path.current + iBECPath)
    try (ipswDFUPath + iBSSPath) +>> (Path.current + iBSSPath)
} catch {
    print("Failed to extract iBEC/iBSS".error, error)
    exit(1)
}

print("Verifying iBEC/iBSS... ".pretty, terminator: "")
if !verifyFiles(at: [iBECPath, iBSSPath], print: true) {
    print("Failed to verify iBEC/iBSS".error)
    exit(1)
}
